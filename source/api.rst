.. _api:

API
===

.. module:: core

This part of the documentation covers all of the REST endpoints for the
application.
