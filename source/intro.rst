.. _introduction:

Introduction
============

The Hawk application was developed to provide a modular and unified experience
for the employees of Stanley Black & Decker.
