User's Guide
------------

This section of the documentation focuses on step-by-step directions on how to
use the frontend of the application.

.. toctree::
   :maxdepth: 2

   intro
   modules

API Reference
-------------

If you are looking to interface with the backend directly, this section is for
you.

.. toctree::
   :maxdepth: 2

   api
