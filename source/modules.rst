.. _modules

Modules
=======

Modules are functionality encapsulations for the application. All of the
functionality of the application can be classified into one of the many modules
that have been implemented. To extend the application, generally a new module
is created. That is not to say that the current modules will not change in
scope. This modularity allows the application to serve various purposes across
the organization.

Modularity allows us to restrict the amount of information delivered to any one
user. We can create a strong foundation that is feature independent and extend
functionality into diverse fields (such as Human Resources and Industry 4.0)
without blending experiences. Each user can have a customized experience that
will include relevant and useful information.

After creating an account, a user can request access to modules. Admin users
can also apply changes (including modules) to user accounts that they manage.

Abstract Module
---------------

An Abstract Module is not implemented itself but defines general guidelines on
how a module is constructed included common features and data.

Included data:

- Roles

Core
----

The Core module implements all of the standard features. These features often
form a basis for other modules. For example, Site creation is considered a part
of the Core module, but sites are often used in other modules. Other modules
may even extend the functionality of these features, but will be treated as
distinct. That is the EHS module implements departments as defined by EHS
managers. These departments are specific to the EHS module but relate to sites.

Included data:

- Users
- Sessions
- Sites
- Languages
- Modules
- Requests

EHS
---

The EHS (Environment, Health, and Safety) module includes form submissions
related to safety checks. These submissions can be retrieved by managers and
used to bolster safety.

Included data:

- ASSIST
- Near Miss
