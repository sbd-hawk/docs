.. rst-class:: hide-header

.. rst-class:: hide-header

Welcome to Hawk
===============

.. image:: _static/full.png
    :alt: Hawk: all things Industry 4.0
    :align: right
    :width: 400
    :target: https://www.gitlab.com/sbd-hawk/hawk-api

Welcome to Hawk's documentation.
Hawk is a modular, extensible web application for Industry 4.0.
Get an overview with :ref:`quickstart` or, if you are a maintainer, get started with :ref:`installation`.
There are also detailed :ref:`tutorials` showing how to do various tasks.
The rest of the docs describe the components of Hawk in detail.
Note: these docs are still a work in progress.

.. include:: contents.rst.inc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
